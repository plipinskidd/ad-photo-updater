# ---------------------------------------------------------------------------------------------
#
# Paul's AD image updater v0.10
# 
# Provides a simple GUI to easily modify a user's AD thumbnail photo (used for
# a user's Teams/Exchange icon).  Will automatically optimize and resize overly-large images.
#
# ---------------------------------------------------------------------------------------------
# v10 note: Added image resizing and saving to AD.
#
# Note for v11: See https://learn.microsoft.com/en-us/exchange/troubleshoot/administration/user-photos-not-synced-to-exchange-online
#       regarding max filesizes for thumbnailphotos (100kb max on-prem, but only 10kb for it to sync to Exchange from AzureAD!
#       96x96px recommended resolution, but most images of this size are over 10kb. What do?)


# ------ BEGIN ------


$DEBUG = 1              # Set more verbose debug output in GUI console on or off
$targetSize = 50KB      # Resize photos to less than this value so they'll fit in the AD database (AD accepts 100KB max, but that's huge overkill)


# Add a GUI
#
Add-Type -AssemblyName System.Windows.Forms
[System.Windows.Forms.Application]::EnableVisualStyles()


# Create and store a Base64-encoded blank grey image:
#
$base64Image = 'iVBORw0KGgoAAAANSUhEUgAAAJYAAACWCAIAAACzY+a1AAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAAFiUAABYlAUlSJPAAAAFYSURBVHhe7dEBDQAADMOg+7c1Y9dBUixwC65CXoW8CnkV8irkVcirkFchr0JehbwKeRXyKuRVyKuQVyGvQl6FvAp5FfIq5FXIq5BXIa9CXoW8CnkV8irkVcirkFchr0JehbwKeRXyKuRVyKuQVyGvQl6FvAp5FfIq5FXIq5BXIa9CXoW8CnkV8irkVcirkFchr0JehbwKeRXyKuRVyKuQVyGvQl6FvAp5FfIq5FXIq5BXIa9CXoW8CnkV8irkVcirkFchr0JehbwKeRXyKuRVyKuQVyGvQl6FvAp5FfIq5FXIq5BXIa9CXoW8CnkV8irkVcirkFchr0JehbwKeRXyKuRVyKuQVyGvQl6FvAp5FfIq5FXIq5BXIa9CXoW8CnkV8irkVcirkFchr0JehbwKeRXyKuRVyKuQVyGvQl6FvAp5FfIq5FXIq5BXIa9CXoW8CnkV8irEbQ83DOO9PjdCVAAAAABJRU5ErkJggg=='
$imageByteArray = [Convert]::FromBase64String($base64Image)         # Convert the encoded image data to a byte array
$memoryStream = New-Object System.IO.MemoryStream                   # Create a Memorystream object 
$memoryStream.Write($imageByteArray, 0, $imageByteArray.Length)     # Write the image to it
#$imageByteArray | ForEach-Object {$memoryStream.WriteByte($_)}     # A clunky and slow alternate method for writing to the stream, more suited for multiple objects. Keeping it here because it was interesting!
$blankImage = New-Object System.Drawing.Bitmap($memoryStream)       # Create a new image object and load the image from the MemoryStream


# Define GUI objects.
#
$LoadProfileButton               = New-Object system.Windows.Forms.Button
$LoadProfileButton.text          = "Load Profile"
$LoadProfileButton.width         = 95
$LoadProfileButton.height        = 25
$LoadProfileButton.location      = New-Object System.Drawing.Point(140,25)
$LoadProfileButton.Font          = New-Object System.Drawing.Font('Verdana',10)

$CurrentPictureBox               = New-Object system.Windows.Forms.PictureBox
$CurrentPictureBox.width         = 150
$CurrentPictureBox.height        = 150
$CurrentPictureBox.location      = New-Object System.Drawing.Point(25,65)
$CurrentPictureBox.Image         = $blankImage
$CurrentPictureBox.SizeMode      = [System.Windows.Forms.PictureBoxSizeMode]::zoom

$NewPictureBox                   = New-Object system.Windows.Forms.PictureBox
$NewPictureBox.width             = 150
$NewPictureBox.height            = 150
$NewPictureBox.location          = New-Object System.Drawing.Point(215,65)
$NewPictureBox.Image             = $blankImage
$NewPictureBox.SizeMode          = [System.Windows.Forms.PictureBoxSizeMode]::zoom

$UsernameTextBox                 = New-Object system.Windows.Forms.TextBox
$UsernameTextBox.multiline       = $false
$UsernameTextBox.text            = "Username"
$UsernameTextBox.width           = 100
$UsernameTextBox.height          = 20
$UsernameTextBox.location        = New-Object System.Drawing.Point(25,25)
$UsernameTextBox.Font            = New-Object System.Drawing.Font('Verdana',10)

$BrowseButton                    = New-Object system.Windows.Forms.Button
$BrowseButton.text               = "Browse..."
$BrowseButton.enabled            = $false   # Start disabled
$BrowseButton.width              = 70
$BrowseButton.height             = 30
$BrowseButton.location           = New-Object System.Drawing.Point(380,65)
$BrowseButton.Font               = New-Object System.Drawing.Font('Microsoft Sans Serif',10)

$UpdateButton                    = New-Object system.Windows.Forms.Button
$UpdateButton.text               = "Update"
$UpdateButton.enabled            = $false   # Start disabled
$UpdateButton.width              = 70
$UpdateButton.height             = 30
$UpdateButton.location           = New-Object System.Drawing.Point(380,110)
$UpdateButton.Font               = New-Object System.Drawing.Font('Microsoft Sans Serif',10)

$CurrentImageText                = New-Object system.Windows.Forms.Label
$CurrentImageText.text           = "Current image"
$CurrentImageText.AutoSize       = $true
$CurrentImageText.width          = 25
$CurrentImageText.height         = 10
$CurrentImageText.location       = New-Object System.Drawing.Point(25,220)
$CurrentImageText.Font           = New-Object System.Drawing.Font('Microsoft Sans Serif',10)

$NewImageText                    = New-Object system.Windows.Forms.Label
$NewImageText.text               = "New image"
$NewImageText.AutoSize           = $true
$NewImageText.width              = 25
$NewImageText.height             = 10
$NewImageText.location           = New-Object System.Drawing.Point(215,220)
$NewImageText.Font               = New-Object System.Drawing.Font('Microsoft Sans Serif',10)

$ResultTextBox                   = New-Object system.Windows.Forms.TextBox
$ResultTextBox.text              = "Enter a username and press Enter`r`n"
$ResultTextBox.multiline         = $true
$ResultTextBox.Scrollbars        = "Vertical" 
$ResultTextBox.ReadOnly          = $true
$ResultTextBox.width             = 425
$ResultTextBox.height            = 100
$ResultTextBox.location          = New-Object System.Drawing.Point(25,255)
$ResultTextBox.Font              = 'Verdana,10'

$Form                            = New-Object system.Windows.Forms.Form
$Form.ClientSize                 = New-Object System.Drawing.Point(475,375)
$Form.text                       = "AD profile picture updater"
$Form.TopMost                    = $false
$Form.MaximizeBox                = $false              # Remove the maximize button.
$Form.AcceptButton               = $LoadProfileButton  # Pressing Enter presses this button.
$Form.FormBorderStyle            = 'Fixed3D'           # Disable window resize.


# Add the objects which are defined above to the panel. The order determines which first gets
# focus, and tab key ordering.
#
$Form.controls.AddRange(
    @(
        $UsernameTextBox,  # Placing the username textbox first puts the cursor in this box by default.
        $LoadProfileButton,
        $BrowseButton,
        $UpdateButton,
        $CurrentPictureBox,
        $NewPictureBox,
        $CurrentImageText,
        $NewImageText,
        $ResultTextBox
     )
)

# Assign actions and functions to the interactable GUI objects.
#
$LoadProfileButton.Add_Click({ LoadProfile })
$BrowseButton.Add_Click({ LoadNewImage })
$UpdateButton.Add_Click({ SaveImage })


# Functions to display output in the GUI console window:
#
Function Print {
        $ResultTextBox.AppendText($args + "`r`n")
}

Function Debug {
    if ($DEBUG){
        $ResultTextBox.AppendText($args + "`r`n")
    }
}

# Function to get/display a user's profile picture
#
Function LoadProfile(){
    
    # Store the username now so changing the textbox after image load doesn't upload to the wrong user
    $Global:userToChange = $UsernameTextBox.Text
    
    # Clear the New Picture box from any previous uploads
    $NewPictureBox.Image = $blankImage
    
    Print "Getting photo for user" $UsernameTextBox.Text "..."
    $user = Get-ADUser $UsernameTextBox.Text -Properties thumbnailPhoto
    
    # Ensure the user exists in AD
    if ($user){
        
        # Get the thumbnail photo from the user's object:
        $photo = $user.thumbnailPhoto
        
        # Ensure the user has a photo
        if ($photo){
            
            # Store the thumbnail photo in memory
            $stream = New-Object System.IO.MemoryStream         # Create a memory stream and store the image
            $stream.Write($photo, 0, $photo.Length)             # Write the image to it
            #$stream.Position = 0
            $bitmap = New-Object System.Drawing.Bitmap($stream) # Create a new image object and load the image from the MemoryStream
            $CurrentPictureBox.Image = $bitmap                  # Display the stored image
            Debug "Width is:" $bitmap.Width "Height is:" $bitmap.Height
            Print "Done."
            $BrowseButton.enabled = $true                       # Enable browse for new picture button
            $UpdateButton.enabled = $false                      # We've not yet picked a file to upload, so disable the button

        }
        
        else {
            Print "User" $UsernameTextBox.Text "exists, but has no photo in AD!"
            $CurrentPictureBox.Image = $blankImage
            $BrowseButton.enabled = $true
            $UpdateButton.enabled = $false
        }
    }
    
    else {
        Print "ERROR: User" $UsernameTextBox.Text "does not exist in AD!"
        $CurrentPictureBox.Image = $blankImage
        $BrowseButton.enabled = $false      # Don't allow uploads to a non-existent user
    }
}


# Function to choose a new AD image for upload
#
Function LoadNewImage(){

    $Global:FileBrowser = New-Object System.Windows.Forms.OpenFileDialog -Property @{
        #InitialDirectory = [Environment]::GetFolderPath('Desktop')
        RestoreDirectory = $false
        Filter = "JPG/PNG files|*.jpg;*.jpeg;*.png"
        }
    
    $result = $FileBrowser.ShowDialog()
    
    if ($result -eq "OK"){
        
        Debug "Loaded" $FileBrowser.FileName
        # TODO: Add image validation (Check for jpg/png headers)
        $NewPictureBox.imageLocation = $FileBrowser.FileName    # Display the prospective image
        $UpdateButton.enabled = $true                           # We now have a filename, so enable the Update button
    }

    else {
        Debug 'Selection cancelled.'
    }
}


# Function to resize image (if necessary) and send to AD. Images with transparency (such as our
# circular survey photos that might be used for AD) appear black in the transparent areas when
# flattened by AD. We underlay all transparency with white to avoid this.
#
# (TODO: Split this into multiple functions (Transparency fix, resize, save to AD)
#
Function SaveImage(){

    # Load the image from the selected file as a bitmap
    $origImage = New-Object System.Drawing.Bitmap -ArgumentList $FileBrowser.FileName
   
    # Get the starting width and height
    $targetWidth = $origImage.Width
    $targetHeight = $origImage.Height

    # Create a new bitmap for the combined image with the same dimensions as the original image
    $combinedImage = New-Object System.Drawing.Bitmap -ArgumentList $targetWidth, $targetHeight

    # Reduce the width and height until the resulting file size is just under the target size
    while (($targetWidth -gt 0) -and ($targetHeight -gt 0)) {
        
        # Create a white background with the current target dimensions
        $whiteBackground = New-Object System.Drawing.Bitmap($targetWidth, $targetHeight)

        # Create a graphics object from the white background
        $graphics = [System.Drawing.Graphics]::FromImage($whiteBackground)

        # Fill the graphics object with solid white
        $whiteBrush = New-Object System.Drawing.SolidBrush([System.Drawing.Color]::White)
        $graphics.FillRectangle($whiteBrush, 0, 0, $targetWidth, $targetHeight)

        # Draw the original image onto the white background
        $graphics.DrawImage($origImage, 0, 0, $targetWidth, $targetHeight)

        # Save the combined image to a memory stream
        $memoryStream = New-Object System.IO.MemoryStream
        $whiteBackground.Save($memoryStream, [System.Drawing.Imaging.ImageFormat]::Jpeg)

        # Check the size of the memory stream
        if ($memoryStream.Length -lt $targetSize) {

            # The size is under the target size, so we're done
            $combinedImage = $whiteBackground
            break
        }
        
        else {
            
            # The size is over the target size, so reduce the width and height by 10% and try again
            $targetWidth = [Math]::Round($targetWidth * 0.9) -as [Int]
            $targetHeight = [Math]::Round($targetHeight * 0.9) -as [Int]
            Debug "Resizing: Width" $targetWidth "Height" $targetHeight "-" $([math]::Round(($memoryStream.Length/1024),1)) "kB"
        }
    }
    
    Debug "Resize complete! New image is:" $([math]::Round(($memoryStream.Length/1024),1)) "kB," $targetWidth "Width" $targetHeight "Height"
        
    # Get the image from the memorystream as a byte array for AD to ingest
    $imageToUpload = $memoryStream.GetBuffer()
    
    # Update AD
    Print "Uploading to AD..."
    Set-ADUser $userToChange -Replace @{thumbnailPhoto=$imageToUpload}
    Print "Done!"   
}


# -------- INIT --------

# Check for existence of the RSAT Active Directory module, and halt if not installed
#
try{
    Get-Command -Name Get-ADUser -ErrorAction Stop | Out-Null
    Get-Command -Name Set-ADUser -ErrorAction Stop | Out-Null
}
catch{
    $LoadProfileButton.enabled = $false
    $ResultTextBox.Clear()
    Print "STOP! The RSAT Active Directory module is not installed. You must run this script from your jumpbox. If you are, install the module from an elevated Powershell command line with:"
    Print "Add-WindowsCapability -Name Rsat.ActiveDirectory.DS-LDS.Tools~~~~0.0.1.0 -Online"
}

# Show the GUI we built at the beginning of the script.
#
[void]$Form.ShowDialog()
