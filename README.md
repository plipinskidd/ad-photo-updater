# AD Photo Updater

Provides a simple GUI to easily modify a user's AD thumbnail photo (used for a user's Teams/Exchange icon).  Will automatically optimize and resize overly-large images.
